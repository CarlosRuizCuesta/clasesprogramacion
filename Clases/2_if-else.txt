-------¿Que es el IF?-------

Es una estructura de control utilizada para tomar decisiones. Es un condicional que sirve para realizar unas u otras operaciones en función de una expresión. Funciona de la siguiente manera, primero se evalúa una expresión, si da resultado positivo se realizan las acciones relacionadas con el caso positivo.

-------Sintaxis-------

if (expresión) { 
   //sentencia1
}

expresión: Una condición que puede ser evaluada como verdadera o falsa.
sentencia1: Sentencia que se ejecutará si condición es evaluada como verdadera.

Vemos como con unas llaves engloban las acciones que queremos realizar en caso de que se cumplan las expresiones.


Opcionalmente se pueden indicar acciones a realizar en caso de que la evaluación de la sentencia devuelva resultados negativos.

if (expresión) { 
	//sentencia1
} else { 
	//sentencia2
}

else: Palabra reservada que ejecuta la sentencia2 encaso de que el resultado de la expresion sea falso.
sentencia2: Sentencia que se ejecutará si condición es evaluada como falsa.

Otro detalle que salta a la vista es el sangrado (margen) que hemos colocado en cada uno de los bloques de instrucciones a ejecutar en los casos positivos y negativos. Este sangrado es totalmente opcional, aunque yo lo recomiendo puesto que facilita la lectura del codigo, y asi se suele exigir en todos los puestos de trabajo.


-------Expresiones condicionales-------

La expresión a evaluar se coloca siempre entre paréntesis y está compuesta por variables que se combinan entre si mediante operadores condicionales. En JavaScript disponemos de los operadores lógicos habituales en lenguajes de programación como son “es igual”, “es distinto”, menor, menor o igual, mayor, mayor o igual, and (y), or (o) y not (no).

Es igual 	: == (a==b)
Es distinto : != (a!=b)
Menor 		: <  (a<b)
Menor igual	: <= (a<=b)
Mayor 		: >  (a>b)
Mayor igual : >= (a>=b)

Ejemplos:

if (edad > 18) {
	document.write("puedes ver esta página para adultos")
}



-------expresiones lógicas-------

Los operadores lógicos se usan para combinar dos valores Booleanos y devolver un resultado verdadero. Es decir, compara dos expresiones y devuelve un resultado booleano dependiendo de la como se comparen y el valor de las mismas. Los operadores mas usados son el Y y el O. 

Operador Y : Devuelve True cuando Expresión1 y Expresión2 son verdaderas. 

Ejemplo:

if (true && true) {
	sentencia1
}


Operador OR : Devuelve True cuando Expresión1 o Expresión2 es verdadera.

Ejemplo:

if (true || false) {
	sentencia1
}


Ejemplo de codigo para operador Y:

Comprobar si la batería de nuestro supuesto ordenador es menor que 0.5 (está casi acabada) y también comprobamos si el ordenador no tiene red eléctrica (está desenchufado). 

if (bateria < 0.5 && redElectrica == 0) {
   document.write("tu ordenador portatil se va a apagar en segundos")
}

Ejemplo de codigo para operador OR:

Comprar si nuestro ordenador puede arrancar comprobando si tiene bateria (mas de 0) o si esta conectado a la red electrica

if (bateria > 0 || redElectrica > 0) {
   document.write("tu ordenador portatil tiene la energia suficiente para encenderse")	
}


-------Sentencias IF anidadas-------

Para hacer estructuras condicionales más complejas podemos anidar sentencias IF, es decir, colocar estructuras IF dentro de otras estructuras IF. Con un solo IF podemos evaluar y realizar una acción u otra según dos posibilidades, pero si tenemos más posibilidades que evaluar debemos anidar IFs para crear el flujo de código necesario para decidir correctamente.

Determinar si un número es cero, positivo o negativo.

if (num == 0) {
	document.write("El número es cero")
} else {
	if (num > 0) {
		document.write("El número es mayor que 0")
	} else {
		document.write("El número es menor que 0")
	}
}